import sys
import time
from pymongo import MongoClient
import json
from bson import json_util
import pandas as pd
import nltk
from utils import tokenize_normalize
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
import math
import joblib

NUM_CLUSTERS = 10

df = pd.read_pickle("df.pkl")
print(df.shape)
print(df.head())

start = time.time()
text = df["full_text"]
tfidf_vectorizer = TfidfVectorizer(max_df=0.5, stop_words='english', use_idf=True, tokenizer=tokenize_normalize, max_features=100000)
tfidf_matrix = tfidf_vectorizer.fit_transform(text)
end = time.time()

print(f'Time to perform Tfidf: {round(end - start)}')
print(tfidf_matrix.shape)

start = time.time()
km = KMeans(n_clusters=NUM_CLUSTERS)
km.fit(tfidf_matrix)
joblib.dump(km, "cluster.pkl")
end = time.time()
print(f'Time to perform K-means: {round(end - start)}')
print("cluster.pkl saved")