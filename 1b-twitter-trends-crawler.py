import tweepy
import sys
import csv
import pandas

access_key = "1234605077396148225-WdFI38JeKRDwvvSXUutBcSDFDrYmGg"
access_secret = "w918L6UXeDI71AsluQsAw73fGeqKDoBX9dV3T8tuqaKdT"
consumer_key = "3VRYiLHwpgFDAcu2VNOGtEVd2"
consumer_secret = "MKWEr4mQjvCuytmnXUr9M8LjwgpFctQEv9NvcsTCju2ULlVkYA"
WOEID_UK = 23424975

# Authorisation and API endpoint
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_key, access_secret)
api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

try:
    api.verify_credentials()
    print("Authentication OK")
except:
    print("Error during authentication")

import time

def change_timestamp(tweet):
    ts = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(tweet['created_at'],'%a %b %d %H:%M:%S +0000 %Y'))
    tweet['created_at'] = ts
    return tweet

df = pandas.read_csv("trends.csv")
trends_names = df["name"].tolist()
trends_names.reverse()

print(trends_names)

from pymongo import MongoClient
import json
import tweepy

MONGO_HOST = "localhost"
DATABASE = "twitter_db"
COLLECTION = "twitter_trends"

try:
    client = MongoClient(MONGO_HOST)
    db = client[DATABASE]
except:
    print("Error Loading DB")
    sys.exit(1)

i = 1

for trend in trends_names:
    #print(trend)
    for tweet in tweepy.Cursor(api.search, q=trend, lang='en', tweet_mode='extended').items(200):
        i += 1
        tweet = tweet._json
        tweet = change_timestamp(tweet)
        db[COLLECTION].insert(tweet)
        print(i, "Tweet saved: ", trend)
        time.sleep(0.2)
        
        """
        tweet = tweet._json
        if(tweet['retweeted'] == False):
            tweet['created_at'] = change_timestamp(tweet)
            db[COLLECTION].insert_one(tweet)
            print(trend)
            print("Tweet saved successfully")
        """