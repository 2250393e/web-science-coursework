# Tweet Crawling

The purpose of this project is to use Twitter APIs to crawl for tweets and extract information from them. 

Requirements for this project are python3 for running scripts and pip3 for install python packages. For the crawling and data organisation part (scripts starting with 1 or 2), the Tweets are meant to be used with a MongoDB database and so this needs to be set up in the same directory as the project. For scripts starting with 3 or 4, they use the serialized dataset ```df.pkl```. You can use this one or generate your own. 

In the directory of the project, install the requirements:
```
pip3 install -r requirements.txt
```

Also install the spacy model used in Tf-idf:
```
python3 -m spacy download en_core_web_md
```

To run any script:
```
python3 <NAME>.py
```

Some of the scripts can take a few minutes and these times are usually recorded in the form of comments in the files. 