import sys
import time
from pymongo import MongoClient
import json
from bson import json_util
import pandas as pd
import nltk
from utils import tokenize_normalize
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
import math
from sklearn.externals import joblib

MONGO_HOST = "localhost"
DATABASE = "twitter_db"
COLLECTION = "twitter_trends"

try:
    client = MongoClient(MONGO_HOST)
    db = client[DATABASE]
    print("Database connection: OK")
except:
    print("Error: Loading DB")
    sys.exit(1)

try:   
    data_count = count = db[COLLECTION].count()
    data_l = list(db[COLLECTION].find({}))
    if (len(data_l) == data_count):
        print(data_count, "Data documents loaded OK")
    else:
        print("Mismatch in number of documents loaded, please CHECK")
except:
    print("Error: Could not load entire DB correctly")

df = pd.DataFrame(data_l)
size_0 = df.shape[0]

df = df.drop_duplicates(subset="id_str")
size_1 = df.shape[0]
num_duplicates = size_0 - size_1
print(num_duplicates, " duplicates removed")
df = df.sample(n=10000)
df.to_pickle("df.pkl")
print("df.pkl saved")