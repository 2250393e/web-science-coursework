9139 Nodes and 7366 Edges
Maximum degree: 225
Minimum degree: 1
Average degree: 1.61199
Most frequent degree: 1
graph is not connected
Number of connected components: 1993
Average clustering coefficient: 0.00306
Transitivity: 0.00154
the node with id LizSpecht has a degree centrality of 0.02462 which is the maximum of the Graph
the node with id RepRaulRuizMD has a closeness centrality of 0.01992 which is the maximum of the Graph
the node with id adamclarkitv has a betweenness centrality of 0.00919 which is the maximum of the Graph
Top-5 degree centrality: [('LizSpecht', 0.024622455679579776), ('adamclarkitv', 0.017509301816590064), ('XXL', 0.016852702998467937), ('DailyRapFacts', 0.015320639089516306), ('DrDenaGrayson', 0.010724447362661415)]
Top-5 degree closeness: [('RepRaulRuizMD', 0.01992011430796053), ('mhplatten', 0.0196198260176465), ('Bluewaveiscomin', 0.019388323645756867), ('NickKristof', 0.019322774411029187), ('editherin1', 0.019229495950589755)]
Top-5 degree betweeness: [('adamclarkitv', 0.00919109793074797), ('RepRaulRuizMD', 0.007428400423219709), ('NickKristof', 0.00724615758184795), ('mhplatten', 0.007131131222918233), ('timg33', 0.0071215017776267405)]
Time to get centrality stats: 220
Time to draw graph: 229