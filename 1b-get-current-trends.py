import tweepy
import sys
import csv

access_key = "1234605077396148225-WdFI38JeKRDwvvSXUutBcSDFDrYmGg"
access_secret = "w918L6UXeDI71AsluQsAw73fGeqKDoBX9dV3T8tuqaKdT"
consumer_key = "3VRYiLHwpgFDAcu2VNOGtEVd2"
consumer_secret = "MKWEr4mQjvCuytmnXUr9M8LjwgpFctQEv9NvcsTCju2ULlVkYA"
WOEID_UK = 23424975

# Authorisation and API endpoint
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_key, access_secret)
api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

try:
    api.verify_credentials()
    print("Authentication OK")
except:
    print("Error during authentication")

def parse_trends_on_key(trends_JSON, parameter):
    trends_response = trends_JSON[0]
    trends = trends_response["trends"]
    trends_p = []

    for trend in trends:
        t = trend[parameter]
        if (t == None):
            trends_p.append("None")
        else:
            if(type(t) == int):
                trends_p.append(str(t))
            else:
                trends_p.append(t)
    
    trends_unicode = ','.join(trends_p)
    trends_str = trends_unicode.encode(encoding='ascii',errors='ignore')
    trends_list = trends_str.split(",")
    return trends_list

try:
    trends_JSON = api.trends_place(WOEID_UK)
    names = parse_trends_on_key(trends_JSON, "name")
    urls = parse_trends_on_key(trends_JSON, "url")
    promoted = parse_trends_on_key(trends_JSON, "promoted_content")
    queries = parse_trends_on_key(trends_JSON, "query")
    volumes = parse_trends_on_key(trends_JSON, "tweet_volume")

    with open('trends.csv', 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        writer.writerow(["name", "url", "promoted_content", "query", "tweet_volume"])

        for i in range(0, len(names)):
            writer.writerow([names[i], urls[i], promoted[i], queries[i], volumes[i]])

except Exception as e:
    print (e)