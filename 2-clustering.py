import pandas as pd
import joblib

"""
['_id' 'created_at' 'id' 'id_str' 'full_text' 'truncated'
 'display_text_range' 'entities' 'metadata' 'source'
 'in_reply_to_status_id' 'in_reply_to_status_id_str' 'in_reply_to_user_id'
 'in_reply_to_user_id_str' 'in_reply_to_screen_name' 'user' 'geo'
 'coordinates' 'place' 'contributors' 'retweeted_status' 'is_quote_status'
 'retweet_count' 'favorite_count' 'favorited' 'retweeted'
 'possibly_sensitive' 'lang' 'quoted_status_id' 'quoted_status_id_str'
 'extended_entities' 'quoted_status' 'withheld_in_countries' 'cluster']
"""
def most_frequent(List): 
    return max(set(List), key = List.count)

df = pd.read_pickle("df.pkl")
df = df.drop_duplicates(subset="id_str")
km = joblib.load("cluster.pkl")

clusters = km.labels_.tolist()
df["cluster"] = clusters
df.to_pickle("df_cluster.pkl")
cluster_counts = df["cluster"].value_counts().sort_index()

for i in range(0, 10):
    df_c = df.loc[df["cluster"] == i]

    entities = df_c["entities"]
    hashtags = {}
    user_mentions = {}

    for e in entities:
        if(len(e["hashtags"]) > 0):
            ht = e["hashtags"][0]["text"]
            if (ht in hashtags):
                hashtags[ht] += 1
            else:
                hashtags[ht] = 1
        if (len(e["user_mentions"]) > 0):
            mention  = e["user_mentions"][0]["screen_name"]
            if (mention in user_mentions):
                user_mentions[mention] += 1
            else:
                user_mentions[mention] = 1
    
    sorted_ht = sorted(hashtags, key = hashtags.get, reverse = True)
    sorted_um = sorted(user_mentions, key = user_mentions.get, reverse = True)

    df_users = df_c["user"]
    users = {}

    for u in df_users:
        user = u["screen_name"]
        if (user in users):
            users[user] += 1
        else:
            users[user] = 1
    
    sorted_u = sorted(users, key = users.get, reverse = True)

    
    print("---")
    print("Cluster", i)
    print("Number of tweets:", df_c.shape[0])
    print("Number of accounts:", len(users.keys()))
    print("Hashtags:", sorted_ht[:5])
    print("Users:", sorted_u[0:5])
    print("Mentions:", sorted_um[0:5])