"""
['_id' 'created_at' 'id' 'id_str' 'full_text' 'truncated'
 'display_text_range' 'entities' 'metadata' 'source'
 'in_reply_to_status_id' 'in_reply_to_status_id_str' 'in_reply_to_user_id'
 'in_reply_to_user_id_str' 'in_reply_to_screen_name' 'user' 'geo'
 'coordinates' 'place' 'contributors' 'retweeted_status' 'is_quote_status'
 'retweet_count' 'favorite_count' 'favorited' 'retweeted'
 'possibly_sensitive' 'lang' 'quoted_status_id' 'quoted_status_id_str'
 'extended_entities' 'quoted_status' 'withheld_in_countries' 'cluster']
"""

import sys
import pandas as pd
import networkx as nx
from matplotlib import pyplot as plt
import numpy as np
from scipy import stats
from operator import itemgetter
import time
from matplotlib.lines import Line2D

df = pd.read_pickle("df.pkl")
df = df.drop_duplicates(subset="id_str")

tweeters = []
user_2_hashtags = {}

for i in range(0, df.shape[0]):
    tweeter_node = df.iloc[i]["user"]["screen_name"]
    tweeters.append(tweeter_node)
    
    hashtag1 = df.iloc[i]["entities"]["hashtags"]

    if(len(hashtag1) > 0):
        hashtag2 = (hashtag1[0]["text"])
        #print(tweeter_node, "uses #"+(hashtag2))
        
        if (tweeter_node in user_2_hashtags.keys()):
            if(hashtag2 not in user_2_hashtags[tweeter_node]):
                user_2_hashtags[tweeter_node].append(hashtag2)
        else:
            user_2_hashtags[tweeter_node] = [hashtag2,]

hashtag_2_frequency = {}

# Takes 30 seconds
start = time.time()

G_coocurrence = nx.Graph()

for user1 in user_2_hashtags.keys():
    for user2 in user_2_hashtags.keys():
        if (user1 == user2):
            continue
        else:
            ht1 = user_2_hashtags[user1]
            ht2 = user_2_hashtags[user2]
            
            for ht in ht1:
                if ht in ht2:
                    if (G_coocurrence.has_edge(user1, user2)):
                        G_coocurrence[user1][user2]['weight'] += 1
                    else:
                        G_coocurrence.add_edge(user1, user2, weight=1)
                        if (ht in hashtag_2_frequency.keys()):
                            hashtag_2_frequency[ht] += 1
                        else:
                            hashtag_2_frequency[ht] = 1


end = time.time()
print(f'Time to make graph: {round(end - start)}')


# Takes 3 minutes
start = time.time()
degrees = [val for (node, val) in G_coocurrence.degree()]
print("---")
print(f"{G_coocurrence.number_of_nodes()} Nodes and {G_coocurrence.number_of_edges()} Edges")
print(f"Maximum degree: {np.max(degrees)}")   
print(f"Minimum degree: {np.min(degrees)}")
print(f"Average degree: {np.mean(degrees):.5f}")  
print(f"Most frequent degree: {stats.mode(degrees)[0][0]}")

if nx.is_connected(G_coocurrence):
    print("graph is connected")
else:
    print("graph is not connected")

print(f"Number of connected components: {nx.number_connected_components(G_coocurrence)}")
print(f"Average clustering coefficient: {nx.average_clustering(G_coocurrence):.5f}")

sorted_hashtags = sorted(hashtag_2_frequency.items(), key = itemgetter(1), reverse = True)
top = sorted_hashtags[0:10]
print("Top-shared hashtags:", sorted_hashtags[0:10])
print("TOTAL;", len(sorted_hashtags))

end = time.time()
print(f'Time to get stats: {round(end - start)}')
"""

# Takes 5-6 minutes
start = time.time()
node_and_degree = G_coocurrence.degree()
colors_central_nodes = ['red', 'green', 'yellow', 'blue', 'brown']
central_nodes = ['COVID19', 'InternationalWomenDay2020', 'EmployeeAppreciationDay', 'coronavirus', 'TakeATuneToTheDentist']
pos = nx.spring_layout(G_coocurrence, k=0.05)
plt.figure(figsize = (20,20))
nx.draw(G_coocurrence, pos=pos, node_color=range(4433), cmap=plt.get_cmap('Spectral'), edge_color="black", linewidths=0.3, node_size=60, alpha=0.6, with_labels=False)
nx.draw_networkx_nodes(G_coocurrence, pos=pos, node_size=300)
plt.savefig('graph_hashtag_coocurence.png')
end = time.time()
print(f'Time to draw graph: {round(end - start)}')
"""

for i in range(0, len(top)):
    ht = str(top[i][0])
    edges = str(top[i][1])
    percentage = round(int(edges) / 782624 * 100, 2)
    percentage = str(percentage)
    print(ht, edges, "("+percentage+"%)")